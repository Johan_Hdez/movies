import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import 'rxjs/Rx';
import 'rxjs/add/operator/map'
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';

import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MoviesService {

  constructor(private http: HttpClient) { }

  public getApi(): Observable<any> {
    return this.http.get(environment.url)
      .map(response => response)
      .catch(error => { 
        return this.handleError(error)
      });
  }

  handleError(error: Response) {
    if (error.status == 500) {      
      console.log('Error interno en el servidor')
    } else {
      return Observable.throw(error);
    }
  }
}
