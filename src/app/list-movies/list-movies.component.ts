import { Component, OnInit } from '@angular/core';
import { MoviesService } from '../services/movies/movies.service';

@Component({
  selector: 'app-list-movies',
  templateUrl: './list-movies.component.html',
  styleUrls: ['./list-movies.component.scss']
})
export class ListMoviesComponent implements OnInit {

  private listmovies: any;
  private results: any;
  constructor(private movies: MoviesService) { }

  ngOnInit() {
    this.getMovies();
  }
  private getMovies() {
    this.movies.getApi().subscribe((res) => {
      this.listmovies = res.results
      this.results = res.results
    });
  }
  private searchMovies(data) {
    if (data) {
      this.listmovies = this.listmovies.filter(object => {
          return object['release_date'] === data;
      });
    } else {
      this.listmovies = this.results
    }
  }
}
